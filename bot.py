import discord
import requests
import json

api = "https://api.vegaconflict.co/player/stats/"
token = ''
client = discord.Client()

def find_player(message):

    query = message.content.split()
    response = requests.get(api + query[1])

    if response.status_code == 200:
        data = (json.loads(response.content.decode('utf-8')))
        embed = discord.Embed(title=data['info']['name'], color=0x50bdfe)
        embed.set_thumbnail(url=data['info']['avatar'])
        embed.add_field(name='Player ID', value=data['id']['playerId'])
        embed.add_field(name='Level', value=data['info']['level'])
        embed.add_field(name='Medals', value=data['info']['medals'])
        embed.add_field(name='Sector', value=data['info']['sector'])
        embed.add_field(name="Base Attack: {} {}%".format(data['base_atk']['total'], "%.2f" % round(data['base_atk']['win_percent'], 2)),
                        value="Won: {}, Drawn: {}, Lost: {}".format(data['base_atk']['won'], data['base_atk']['drawn'],data['base_atk']['lost']), inline=False)
        embed.add_field(name="Base Defense: {} {}%".format(data['base_def']['total'], "%.2f" % round(data['base_def']['win_percent'], 2)),
                        value="Won: {}, Drawn: {}, Lost: {}".format(data['base_def']['won'], data['base_def']['drawn'],data['base_def']['lost']), inline=False)
        embed.add_field(name="Fleet Attack: {} {}%".format(data['fleet_atk']['total'], "%.2f" % round(data['fleet_atk']['win_percent'], 2)),
                        value="Won: {}, Drawn: {}, Lost: {}".format(data['fleet_atk']['won'], data['fleet_atk']['drawn'],data['fleet_atk']['lost']), inline=False)
        return embed
    else:
        embed = discord.Embed(title="Player Not Found", description=query,color=0x50bdfe)
        return embed

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('!stats'):
        await client.send_message(message.channel, embed=find_player(message))

@client.event
async def on_ready():
    print("Bot Ready")
client.run(token)